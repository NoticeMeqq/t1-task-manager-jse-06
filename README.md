# TASK MANAGER

## DEVELOPER INFO

**Name**: Sergey Annenkov

**E-mail**: glaatum@gmail.com

## SOFTWARE

**OS**: Windows 10 21H1 19043.1706

**JDK**: OPENJDK 1.8.0_275

## HARDWARE

**CPU**: AMD Ryzen 7 2700x 3.70 GHz

**RAM**: HyperX Predator 16GB 3200 MHz

**SSD**: Samsung 860 Evo 512 GB

## BUILD PROGRAM

```bash
mvn clean install
```

## RUN PROGRAM

### TASK MANAGER COMMANDS LIST

```bash
java -jar ./task-manager.jar -h
```

Or use console command HELP for full list of commands
