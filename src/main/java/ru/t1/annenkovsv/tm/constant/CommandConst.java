package ru.t1.annenkovsv.tm.constant;

public final class CommandConst {

    public static final String HELP = "HELP";

    public static final String VERSION = "VERSION";

    public static final String ABOUT = "ABOUT";

    public static final String EXIT = "EXIT";

    private CommandConst() {
    }

}