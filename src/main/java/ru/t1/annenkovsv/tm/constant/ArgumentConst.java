package ru.t1.annenkovsv.tm.constant;

public final class ArgumentConst {

    public static final String HELP = "-H";

    public static final String VERSION = "-V";

    public static final String ABOUT = "-A";

    private ArgumentConst() {
    }

}